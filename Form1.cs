﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using voteCollecter.Support;

namespace voteCollecter
{
    public partial class Form1 : Form
    {
        private MySqlConnection conn;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Open connection, please remember to shut it down when exits
            string connStr = "server=" + DatabaseInfo.databaseUrl
                + ";user=" + DatabaseInfo.userName
                + ";database=" + DatabaseInfo.databaseName
                + ";port=" + DatabaseInfo.port
                + ";password=" + DatabaseInfo.password
                + ";";

            conn = new MySqlConnection(connStr);

            try
            {
                
                conn.Open();
                // Perform database operations
            }
            catch (Exception ex)
            {
                
            }
            
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
            int numHavePut = textBox1.Text.Length;
            if(numHavePut < UserSettings.totalOfSection[0])
            {
                label4.Text = "目前已经填写" + numHavePut + "题，共有" + UserSettings.totalOfSection[0] + "题";
            }
            else
            {
                label4.Text = "填写完毕";
                textBox2.Focus();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            conn.Close();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
            int numHavePut = textBox2.Text.Length;
            if (numHavePut < UserSettings.totalOfSection[1])
            {
                label5.Text = "目前已经填写" + numHavePut + "题，共有" + UserSettings.totalOfSection[1] + "题";
            }
            else
            {
                label5.Text = "填写完毕";
                textBox3.Focus();
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            
            int numHavePut = textBox3.Text.Length;
            if (numHavePut < UserSettings.totalOfSection[2])
            {
                label6.Text = "目前已经填写" + numHavePut + "题，共有" + UserSettings.totalOfSection[2] + "题";
            }
            else
            {
                label6.Text = "填写完毕";
            }
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                this.button1_Click(textBox3, null);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == UserSettings.totalOfSection[0]
                || textBox2.Text.Length == UserSettings.totalOfSection[1]
                || textBox3.Text.Length == UserSettings.totalOfSection[2])
            {
                char[] array1 = textBox1.Text.ToUpper().ToCharArray();
                char[] array2 = textBox2.Text.ToUpper().ToCharArray();
                char[] array3 = textBox3.Text.ToUpper().ToCharArray();

                string value = "";
                try
                {
                    for (int count = 0; count < array1.Length; count++)
                    {
                        if (array1[count] > 57) { array1[count] = (char)(array1[count] - 16); }
                        if (array1[count] > 48 && array1[count] < 55)
                            value = value + array1[count].ToString() + ",";
                        else
                            throw new Exception("第一部分超出选项范围");
                    }

                    for (int count = 0; count < array2.Length; count++)
                    {
                        if (array2[count] > 57) { array2[count] = (char)(array2[count] - 16); }
                        if (array1[count] > 47 && array1[count] < 53)
                            value = value + array2[count].ToString() + ",";
                        else
                            throw new Exception("第二部分超出选项范围");
                    }

                    for (int count = 0; count < array3.Length; count++)
                    {
                        if (array3[count] > 57) { array3[count] = (char)(array3[count] - 16); }
                        if (array1[count] > 48 && array1[count] < 53)
                            value = value + array3[count].ToString() + ",";
                        else
                            throw new Exception("第三部分超出选项范围");
                    }

                    if (textBox4.Text != "")
                    {
                        value = value + "'no'";
                    }
                    else
                        value = value + "'" + textBox4.Text + "'";

                    string sql = "insert into `littlecreate` (`A1`,`A2`,`A3`,`A4`,`A5`,`A6`,`A7`,`A8`,`A9`,`B1`,`B2`,`B3`,`B4`,`B5`,`B6`,`B7`,`B8`,`B9`,`B10`,`B11`,`B12`,`B13`,`B14`,`B15`,`B16`,`B17`,`B18`,`B19`,`B20`,`B21`,`B22`,`B23`,`B24`,`B25`,`C1`,`C2`,`C3`,`C4`,`C5`,`C6`,`C7`,`C8`,`C9`,`C10`,`C11`,`C12`,`C13`,`C14`,`C15`,`C16`,`C17`,`C18`,`C19`,`C20`,`MARK`)"
                                    + "values(" + value + ");";


                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("数据成功提交，回车直接开始录入下一条。");
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox1.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            else
            {
                MessageBox.Show("请填完一个问卷之后再提交");
            }
        }
    }
}
